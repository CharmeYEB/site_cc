<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Site_CC



## Getting started

To make it easy for you to get started with Projet, here's a list of recommended next steps.



### How to run existing laravel project in xampp code example.

#### Example 1: run laravel project from gitlab


1. Go to the folder application using cd command on your cmd or terminal
2. Run `**composer install**` on your cmd or terminal
3. Copy `.env.example` file to `.env` on the root folder.
 You can type `copy .env.example .env ` if using command prompt** Windows **or `cp .env.example .env` if using terminal **Ubuntu**
4. Open your `.env` file and change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.
By default, the username is root and you can leave the password field empty. (This is for Xampp)
By default, the username is root and password is also root. (This is for Lamp)
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Go to localhost:8000

#### Example 2: start someones laravel project

```
1. composer update --no-scripts  or composer install
2. php artisan key:generate 
3. rename example.env to .env
4. Change database credentials, set debug mode! Change app url
4. php artisan migrate(make sure u have the database running in the server)
5. php artisan config:clear
6. you are good to go!

```

#### Example 3: how to run laravel project in xampp

```
Install XAMPP
Install composer(You can download composer from https://getcomposer.org/download/)
Open cmd(make sure your path is the same as C:\xampp\htdocs>) then
Install laravel GLOBALLY by the command: composer global require laravel/installer
Now hit the command: laravel new your-projectname
Your project is created now to run the project use command: php artisan serve
The URL will be displayed on cmd, copy and use that URL in a browser you can see your laravel project
                 
/*If you have a ready project and you want to run with XAMPP*/
if your laravel project is ready then put a laravel project folder at htdocs(unzip if it's .zip file)
Create a database(you can see database name in .env file) then select the created database and import the .sql file(if .sql file is not provided then you have to create an entire database).
Now your project is ready to run.
Start the Apache and MySQL from XAMPP then hit the php artisan serve command in cmd(make sure your cmd is opened with the same path as your project).

```

# Sample Laravel Project

First install PHP Composer

## Steps For Installing PHP Composer on Ubuntu

### Step 1: Update Local Repository

Start by updating the local repository lists by enter the following in a command line:

``` 
sudo apt-get update 

```

### Step 2: Download the Composer Installer

To download the Composer installer, use the command:

```

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

```

### Step 3: Install PHP Composer

1. Installing PHP Composer requires curl, unzip, and a few other utilities. Install them by entering the following:

```
sudo apt-get install curl php-cli php-mbstring git unzip

```

2. To install to /usr/local/bin. enter:

```
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

```

3. Once the installer finishes, verify the installation:

```
composer --version

```

Output :
```
Composer version 2.0.12 2021-04-01

```


## Installation of Laravel Via Composer

If your computer already has PHP and Composer installed, you may create a new Laravel project by using Composer directly. After the application has been created, you may start Laravel's local development server using the Artisan CLI's serve command:

```
composer create-project laravel/laravel example-app

cd example-app

php artisan serve

```

### Once the installer finishes, verify the installation:

```
php artisan --version

```

output :

```
Laravel Framework 8.6.1

```

## Add dependencies

Run `composer install` to install all dependencies (not yet dependencies).

## Php version used 

 "php": "^7.3|^8.0",

***

# _**TROUBLESHOOTING**_

## Processus de mise à jour de la base de donnée et des migrations

1. `composer update`
2. `composer install`
3. `php artisan migrate` ou `php artisan migrate:fresh`

