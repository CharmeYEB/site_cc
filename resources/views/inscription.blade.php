@extends('layouts.log')

@section('content')
<div class="row justify-content-center">
	<div class="col-md-12 col-lg-10">
		<div class="wrap d-md-flex">
			<div class="text-wrap p-4 p-lg-5 text-center d-flex align-items-center order-md-last">
                <div class="text w-100">
                    <h2>Welcome to login</h2>
                    <p>Don't have an account?</p>
                    <a href="{{ route('connection') }}" class="btn btn-white btn-outline-white">connection</a>
                </div>
			</div>

		    <div class="login-wrap p-4 p-lg-5">
                <div class="d-flex">
                    <div class="w-100">
                        <h3 class="mb-4">Création de compte</h3>
                    </div>
                </div>

				<form method="POST" action="{{ route('inscription') }}" class="signin-form form-detail">
                    @csrf

                    <div class="form-group mb-3">
                        <label class="label" for="name">{{__('Username') }}</label>
                        <input name="name" type="text" class="form-control" placeholder="Username" required>
                    </div>

                    <div class="form-group mb-3">
                            <label class="label" for="name">{{ __('E-Mail Address') }}</label>
                            <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

		            <div class="form-groupe">
		            	<label class="label" for="password">{{__('Mot de passe') }}</label>
		              <input name="password" type="password" class="form-control" placeholder="Password" required>
                      <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
		            </div>

                    <div class="form-group mb-3">
                        <label class="label" for="password">{{__('Confirmer mot de passe') }}</label>
                        <input type="password" class="form-control" name="re_password" id="re_password" placeholder="Password"/>
                    </div>

                    <label class="label" for="password">{{__('Fonction') }}</label>
                    <div class="form-left form-row">
                        <select name="status">
                            <option value="admin">Administrateur</option>
                            <option value="superadmin">Super Administrateur</option>
                            <option value="visitor">Visiteur</option>
                        </select>
                        <span class="select-btn">
                            <i class="zmdi zmdi-chevron-down"></i>
                        </span>
                    </div>


                    <div class="form-group mb-3">
                        <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                        <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                    </div>

		            <div class="form-group">
		            	<button type="submit" class="form-control btn btn-primary submit px-3">Inscription</button>
		            </div>

		        </form>
		    </div>
		</div>
	</div>
</div
@endsection
