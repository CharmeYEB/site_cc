@extends('layouts.log')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="wrap d-md-flex">
                <div class="text-wrap p-4 p-lg-5 text-center d-flex align-items-center order-md-last">
                    <div class="text w-100">
                        <h2>Welcome to login</h2>
                        <p>Don't have an account?</p>
                        <a href="{{ route('inscription') }}" class="btn btn-white btn-outline-white">S'inscrire</a>
                    </div>
                </div>
                <div class="login-wrap p-4 p-lg-5">
                    <div class="d-flex">
                        <div class="w-100">
                            <h3 class="mb-4">Connexion</h3>
                        </div>
                    </div>
                    @if(session()->has('msg'))
                        <div class="alert alert-success">
                            {{ session()->get('msg') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('connection') }}" class="signin-form">
                                @csrf
                        <div class="form-group mb-3">
                            <label class="label" for="name">{{ __('E-Mail Address') }}</label>
                            <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter votre email" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                        <label class="label" for="password">{{ __('Password') }}</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Entrer votre password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary submit px-3">{{ __('Se Connecter') }}</button>
                        </div>
                        <div class="form-group d-md-flex">
                            <div class="w-50 text-left">
                                <label class="checkbox-wrap checkbox-primary mb-0">Remember Me
                                    <input type="checkbox" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="w-50 text-md-right">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div
@endsection
