<!doctype html>
<html lang="en">
  <head>
  	<title>Login </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font Icon -->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/css/montserrat-font.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/login/fonts/material-icon/css/material-design-iconic-font.min.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">


	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="{{ asset('assets/login/css/style.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/login/css/style1.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/login/css/style.css') }}" >

	</head>
<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Connexion </h2>
				</div>
			</div>

            @yield('content')

        </div>
	</section>

	<script src="{{ asset('assets/login/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/login/js/popper.js') }}"></script>
  <script src="{{ asset('assets/login/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/login/js/main.js') }}"></script>
  <script src="{{ asset('assets/login/js/main2.js') }}"></script>
  <script src="{{ asset('assets/login/vendor/jquery/jquery.min.js') }}"></script>

	</body>
