<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', 'App\Http\Controllers\WelcomeController@index')->name('welcome');

Route::get('/presentation', 'App\Http\Controllers\PresentationController@index')->name('presentation');

Route::get('/news', 'App\Http\Controllers\NewsController@index')->name('news');

Route::get('/contact', 'App\Http\Controllers\ContactController@index')->name('contact');

Route::get('/documentation', 'App\Http\Controllers\DocumentationController@index')->name('documentation');

Route::get('/superadmin', 'App\Http\Controllers\SuperadminController@index')->name('superadmin')->middleware('superadmin');

Route::get('/admin', 'App\Http\Controllers\AdminController@index')->name('admin')->middleware('admin');

Route::get('/visitor', 'App\Http\Controllers\VisitorController@index')->name('visitor')->middleware('visitor');

Route::get('/connection', 'App\Http\Controllers\ConnectionController@index')->name('connection');

Route::post('/connection', 'App\Http\Controllers\ConnectionController@action')->name('connection');

Route::get('/inscription', 'App\Http\Controllers\RegisterController@index')->name('inscription');

Route::post('/inscription', 'App\Http\Controllers\RegisterController@action')->name('inscription');

Auth::routes();
