<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

           $users = [
            [

              'name' => 'Super Admin',
              'email' => 'superadmin@gmail.com',
              'password' => '1234567890',
              'status' => 'superadmin',
            ],
            [
              'name' => 'Admin1',
              'email' => 'admin1@gmail.com',
              'password' => '5635640',
              'status' => 'admin',
            ],
            [
              'name' => 'visitor1',
              'email' => 'sisitor1@gmail.com',
              'password' => '0123456',
              'status' => 'visitor',
            ],
             [
              'name' => 'Visiteur 2',
              'email' => 'visiteur2@gmail.com',
              'password' => '9876543210',
              'status' => 'visitor',
            ]
          ];


          foreach($users as $user)
          {
              User::create([
               'name' => $user['name'],
               'email' => $user['email'],
               'status' => $user['status'],
               'password' => Hash::make($user['password'])
             ]);



           }
    }
}
