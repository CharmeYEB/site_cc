<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index()
    {
        return view('inscription');
    }

    public function action(Request $request)
    {
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'status' => $request['status'],
            'password' => Hash::make($request['password']),
        ]);

        return view('connection')->with('msg', 'Inscription réussi, connectez-vous');
    }
}
