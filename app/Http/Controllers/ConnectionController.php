<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConnectionController extends Controller
{
    public function index()
    {
        return view('connection');
    }

    public function action(Request $request)
    {
        $email = $request->email;
        var_dump($email);
        $statut = DB::table('users')->where('email', $email)->first();
        var_dump($statut->status);
        switch ($statut->status) {
             case 'admin':
                 return redirect(route('admin'));
             case 'superadmin':
                 return redirect(route('superadmin'));
             case 'visitor':
                 return redirect(route('visitor'));
             default:
                 return view('connection')->with('email', $email)->with('msg', 'veillez vous inscrire svp');
         }
    }
}
