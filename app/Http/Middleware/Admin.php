<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (auth()->user()->status == 'superadmin') {
            return redirect()->route('superadmin');
        }

        if (auth()->user()->status == 'visitor') {
            return redirect()->route('visitor');
        }

        if (auth()->user()->status == "admin") {

            return $next($request);
        }

        return redirect('/')->with('error','Vous n êtes pas un admin');
    }
}
