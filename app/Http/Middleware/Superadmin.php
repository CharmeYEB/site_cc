<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (auth()->user() && auth()->user()->status == 'superadmin') {

            return $next($request);
        }

        if (auth()->user() && auth()->user()->status == 'visitor') {

            return redirect()->route('visitor');
        }

        if (auth()->user() &&  auth()->user()->status == "admin") {

            return redirect()->route('admin');
        }

        return redirect('/')->with('error','Vous n êtes pas un super admin');
    }
}
